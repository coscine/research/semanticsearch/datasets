This folder contains all data records and files created and used in the master thesis ["An Efficient Semantic Search Engine for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09883). 

## Structure:

- *metadata_records*: Generated exmaple metadata records based on engmeta profile
    - *all*: All 10,000 metadata records, used as large data record
    - *search*: 35 search metadata records, used as small data record
    - *search_focus*: 5 focus search metadata records, used as main target metadata records for the execution of the example search intentions presented in the master thesis 
    - *es_100*: metadata records that are used to test the add/update/delete functionality in the Elasticsearch approach
- *named_graphs*: Contains the turtle files of the named graphs used in the master thesis
    - *changed*: Modified turtle files
    - *general_rules*: General rules used in the master thesis
    - *other*: Additional triples for evaluation
    - *profiles*: Used application profiles
    - *terms*: Created terms for evaluation
    - *vocabularies*: Creaeted vocabularies for evaluation
    - *reused&#46;md*: Reused turtle files from CoScInE
- *virtuoso.db*: Database file for virtuoso 
    - *graphs.db*: Contains all named graphs specified in *named_graphs*
    - *small.db*: Contains all named graphs and search metadata records
    - *large.db*: Contains all named graphs and all metadata records
    - *es_9900.db*: Contains all named graphs and all 9900 metadata records for elasticsearch tests
- *elasticsearch*: Database file for elasticsearch 
    - *small*: Contains all search metadata records
    - *large*: Contains all metadata records
    - *es_9900*: Contains all 9900 metadata records for elasticsearch tests

## How to reuse Virtuoso and Elasticsearch data:

- Virtuoso: 
    - Stop Virtuoso
    - Replace the *virtuoso.db* file in the *database* folder (renaming is necessary)
    - Start Virtuoso
- Elasticsearch: 
    - Stop Elasticsearch
    - Replace folder *0* in the folder *data/nodes*
    - Start Elasticsearch

## License notes:

The files contain data from [DBpedia](https://wiki.dbpedia.org/) about persons, abstracts and titles. 