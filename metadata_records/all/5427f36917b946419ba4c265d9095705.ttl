@base <https://purl.org/coscine/md/5427f36917b946419ba4c265d9095705/>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix engmeta: <http://www.ub.uni-stuttgart.de/dipling#>.
@prefix prov: <http://www.w3.org/ns/prov#>.
@prefix premis3: <http://www.loc.gov/premis/rdf/v3/>.
@prefix dctype: <http://purl.org/dc/dcmitype/>.
@prefix mode: <https://purl.org/coscine/terms/mode#>.
@prefix license: <http://spdx.org/licenses/>.
@prefix format: <https://www.iana.org/assignments/media-types/application/>.

<https://purl.org/coscine/md/5427f36917b946419ba4c265d9095705/> dcterms:abstract """A Short History of Progress is a non-fiction book and lecture series by Ronald Wright about societal collapse. The lectures were delivered as a series of five speeches, each taking place in different cities across Canada as part of the 2004 Massey Lectures which were broadcast on the CBC Radio program, Ideas. The book version was published by House of Anansi Press and released at the same time as the lectures. The book spent more than a year on Canadian best-seller lists, won the Canadian Book Association's Libris Award for Non-Fiction Book of the Year, and was nominated for the British Columbia's National Award for Canadian Non-Fiction. It has since been reprinted in a hardcover format with illustrations. Wright, an author of fiction and non-fiction works, uses the fallen civilisations of Easter Island, Sumer, Rome, and Maya, as well as examples from the Stone Age, to see what conditions led to the downfall of those societies. He examines the meaning of progress and its implications for civilizations—past and present—arguing that the twentieth century was a time of runaway growth in human population, consumption, and technology that has now placed an unsustainable burden on all natural systems. In his analysis of the four cases of fallen civilizations, he notes that two (Easter Island and Sumer) failed due to depletion of natural resources—\"their ecologies were unable to regenerate.\" The other two failed in their heartlands, \"where ecological demand was highest,\" but left remnant populations that survived. He asks the question: \"Why, if civilizations so often destroy themselves, has the overall experiment of civilization done so well.\" For the answer, he says, we must look to natural regeneration and human migration (Wright, 102). While some ancient civilizations were depleting their ecologies and failing, others were rising. Large expanses of the planet were unsettled. The other factor, evident in both Egypt and China, was that due to abundant resources (e.g., topsoil), farming methods (ones that worked with, rather than against, natural cycles), and settlement patterns, these civilizations had greater longevity (103-104). Changes brought on by the exponential growth of human population (at the time of the book's publication, over 6 billion and adding more than 200 million people every three years) and the worldwide scale of resource consumption, have altered the picture, however. Ecological markers indicate that human civilization has now surpassed (since the 1980s) nature's capacity for regeneration. We are now using more than 125% of nature's yearly output. \"If civilization is to survive, it must live on the interest, not the capital of nature\" (129). He concludes that \"now is our chance to get the future right\"—the collapse of human civilization is imminent if we do not act now to prevent it (132).""";
                                                                dcterms:available "2022-02-28+01:00"^^xsd:date;
                                                                dcterms:created "2016-12-17+01:00"^^xsd:date;
                                                                dcterms:creator <http://dbpedia.org/resource/Lotte_Friis>;
                                                                dcterms:issued "2017-07-11+02:00"^^xsd:date;
                                                                dcterms:publisher <http://dbpedia.org/resource/Alaa_Batayneh>;
                                                                dcterms:subject <http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=101>;
                                                                dcterms:title "A Short History of Progress";
                                                                engmeta:mode mode:analysis;
                                                                engmeta:step <https://purl.org/coscine/terms/step#storage>;
                                                                engmeta:version 5 ;
                                                                engmeta:worked false;
                                                                a <https://purl.org/coscine/ap/engmeta2/>;
                                                                <https://purl.org/coscine/terms/projectstructure#isFileOf> <https://purl.org/coscine/vocabularies/resource#resource1>.
