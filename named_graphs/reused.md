The reused files can be found under https://git.rwth-aachen.de/coscine/applicationprofiles:

### Ontologies:
- csmd
- dc/terms
- foaf
- org 
- premis3
- prov

### Terms:
- engmeta
- mode

### Vocabularies:
- dc/dcmitype
- dfg/dfgs
- licenses
- mime
