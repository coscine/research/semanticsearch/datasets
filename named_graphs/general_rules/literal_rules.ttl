@base <https://purl.org/coscine/literalRules/> .

@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix prefixes: <https://purl.org/coscine/prefixes#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix coscineterms: <https://purl.org/coscine/terms#> .
@prefix coscinesearch: <https://purl.org/coscine/terms/search#> .


prefixes:prefixes
	a owl:Ontology ;
	owl:imports sh: ;
    sh:declare [
		sh:prefix "org" ;
		sh:namespace "http://www.w3.org/ns/org#"^^xsd:anyURI ;
	] ;
    sh:declare [
		sh:prefix "foaf" ;
		sh:namespace "http://xmlns.com/foaf/0.1/"^^xsd:anyURI ;
	] ;
    sh:declare [
		sh:prefix "dcterms" ;
		sh:namespace "http://purl.org/dc/terms/"^^xsd:anyURI ;
	] ;
	sh:declare [
		sh:prefix "rdfs" ;
		sh:namespace "http://www.w3.org/2000/01/rdf-schema#"^^xsd:anyURI ;
	] .

<https://purl.org/coscine/literalRules/>
  dcterms:license <https://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://itc.rwth-aachen.de> ;
  dcterms:rights "Copyright © 2020 IT Center, RWTH Aachen University" ;
  dcterms:title "Rules to find literal values for a class instance"@en .


<http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/>
    sh:rule [
    	a sh:SPARQLRule ;
        rdfs:label "Infer labels of an DFG instance" ;
        sh:prefixes prefixes:prefixes ;
		sh:construct """
			CONSTRUCT {
                $this rdfs:label ?label
            } 
            WHERE {                                 
                ?class rdfs:subClassOf* ?superclass .
                ?superclass rdfs:label ?label .
                $this a ?class .   
                FILTER (?superclass != <http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/>)                  
            }
			""" ;
	] .

<http://xmlns.com/foaf/0.1/Person>
    sh:rule [
		a sh:SPARQLRule ;
        rdfs:label "Infer labels of a person: firstName, lastName and name. If name not exists use a combination of first and last name.";              
        sh:prefixes prefixes:prefixes ;
		sh:construct """
			CONSTRUCT {
                $this rdfs:label ?label
            } 
            WHERE {
                {
                    $this foaf:name ?label                
                }
                UNION
                {
                    $this foaf:givenName ?label                
                }
                UNION
                {
                    $this foaf:familyName ?label                
                }
                UNION
                {    
                    $this foaf:givenName ?firstName .
                    $this foaf:familyName ?lastName .
                    BIND(CONCAT(STR(?firstName), " ", STR(?lastName)) as ?label) .    
                    FILTER NOT EXISTS  {
                        SELECT * WHERE  {                           
                            $this foaf:name ?x
                        }
                    }                     
                }
                UNION
                {
                    ?membership a org:Membership .
                    ?membership org:member $this .
                    ?membership org:organization ?organization .
                    ?organization rdfs:label ?label                
                }
                UNION
                {
                    ?membership a org:Membership .
                    ?membership org:member $this .
                    ?membership org:organization ?unit .
                    ?organization org:hasUnit ?unit .
                    ?organization rdfs:label ?label                
                }
            }
			""" ;
	] .
    
<http://www.w3.org/ns/org#OrganizationalUnit>
    sh:rule [
		a sh:SPARQLRule ;
        rdfs:label "Infer labels of an organizational unit" ;
        sh:prefixes prefixes:prefixes ;
		sh:construct """
			CONSTRUCT {
                $this rdfs:label ?label
            }  
            WHERE {    
                {                
                    $this rdfs:label ?label 
                }
                UNION
                {
                    ?organization org:hasUnit $this .
                    ?organization rdfs:label ?label
                }
            }
			""" ;
	] .

<http://www.w3.org/ns/org#FormalOrganization>
    sh:rule [
		a sh:SPARQLRule ;
        rdfs:label "Infer label of an formal organization" ;
		sh:prefixes prefixes:prefixes ;
		sh:construct """
			CONSTRUCT {
                $this rdfs:label ?label
            }    
            WHERE {                    
                $this rdfs:label ?label
            }
		""" ;
	] .

<http://www.w3.org/ns/shacl#NodeShape>
    sh:rule [
		a sh:SPARQLRule ;
        rdfs:label "Infer label for application profile" ;
        sh:prefixes prefixes:prefixes ;
		sh:construct """
			CONSTRUCT {
                $this rdfs:label ?label
            } 
            WHERE {                    
              $this dcterms:title ?label
            }
			""" ;
	] .

coscinesearch:rootClass
    sh:rule [
		a sh:SPARQLRule ;
        rdfs:label "Infer label by rdfs:label for the root class" ;
        sh:prefixes prefixes:prefixes ;
		sh:construct """
			CONSTRUCT {
                $this rdfs:label ?label
            } 
            WHERE {                    
              $this rdfs:label ?label
            }
			""" ;
	] .